require "./point"
class Triangle

  attr_accessor :points

  def initialize(p1, p2, p3)
    result = []
    result << create_line(p1, p2)
    result << create_line(p2, p3)
    result << create_line(p1, p3)
    @points = result.flatten.uniq
  end

  def create_line(p1, p2)
    delta_x = (p1.x - p2.x).abs
    delta_y = (p1.y - p2.y).abs
    if delta_x >= delta_y
      error = 0
      delta_error = delta_y
      y = p1.y
      result = []
      (p1.x..p2.x).each do |x|
        result << Point.new(x, y)
        error += delta_error
        if 2*error >= delta_x
          y += 1
          error += delta_x
        end
      end
      result
    else
      error = 0
      delta_error = delta_x
      x = p1.x
      result = []
      (p1.y..p2.y).each do |y|
        p "#{x}, #{y}, #{p1.x}-#{p1.y}, #{p2.x}-#{p2.y}"
        result << Point.new(x, y)
        error += delta_error
        if 2*error >= delta_y
          x += 1
          error += delta_y
        end
      end
      result
    end
  end

end