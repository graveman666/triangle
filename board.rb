require "./point"
require './triangle'

class Board

  attr_accessor :points

  def initialize
    @points = [
      Point.new(7, 0),
      Point.new(1, 13),
      Point.new(13, 13)
    ]
    @points = Triangle.new(@points[0], @points[1], @points[2]).points
  end

  def plot
    16.times do |y|
      p prepare_string(y)
    end
  end

  private

  def prepare_string(number)
    current_points = @points.select{ |p| p.y == number }
    current_points_xs = current_points.map{ |p| p.x }
    string = ''
    16.times do |x|
      if current_points_xs.include?(x)
        string << '#'
      else
        string << '.'
      end
    end
    string
  end

end