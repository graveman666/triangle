class Point

  attr_accessor :x
  attr_accessor :y

  def initialize(x, y)
    check_position(x)
    check_position(y)
    @x = x
    @y = y
  end

  private

  def check_position(number)
    unless (0..15).include?(number)
      raise "wrong point, x and y of point must be in 0..15"
    end
  end

end